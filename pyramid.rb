# Write a program thats prints a star pyramid.

class Pyramid

  def initialize(height)
    @height = height
  end
  def show_pyramid
      for i in 1..@height
        print " "*(5-i)
        print "*"*(2 * i - 1)
        print "\n"
      end
      for i in 1..@height
        print " "*(i-1)
        print "*"*(2*@height-2*i+1)
        print "\n"
      end
  end
end


print "Please Enter Pyramid Height....!!"
height = gets.to_i

pyramid = Pyramid.new(height)                         # Object Create Pyramid Class
pyramid.show_pyramid                                  #Method Call


