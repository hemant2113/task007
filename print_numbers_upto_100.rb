#   Write a program that prints from 1 to 100.


class NumbersPrint
  def show_numbers
       print "Program for Print 1 to 100\n"
       (1..100).step(1) do |n| 
       puts "#{n}"
  end
  end
end

number = NumbersPrint.new                          # Object Create NumbersPrint
number.show_numbers                                #  Method call



# end of code